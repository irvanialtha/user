`use strict`;

const 
    { service, amqp, mqtt, subscriber, database} = require(`./app_module`);

let start = () => {
    return new Promise(async (resolve, reject) => {
        resolve({
            service : await service(),
            amqp    : await amqp(),
            db      : await database()
        })
     }).then(async (data) => {
        subscriber();
     });
};

start()