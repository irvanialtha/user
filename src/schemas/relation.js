`use strict`;

module.exports = () => {
    db.users.hasMany(db.users_information, {
        foreignKey: 'user_id'
    });

    db.users_information.belongsTo(db.users, {
        foreignKey : `user_id`
    });

    db.users.hasMany(db.users_role, {
        foreignKey: 'user_id'
    });

    db.users_role.belongsTo(db.users, {
        foreignKey : `user_id`
    });

    db.users.hasMany(db.users_password, {
        foreignKey: 'user_id'
    });

    db.users_password.belongsTo(db.users, {
        foreignKey : `user_id`
    });

    db.users_role.belongsTo(db.master_role, {
        foreignKey : `role_id`
    });

    db.users.hasMany(db.users_dashboard, {
        foreignKey: 'user_id'
    });

    db.users_dashboard.belongsTo(db.master_dashboard, {
        foreignKey : `dashboard_id`
    });

    db.users.hasMany(db.client_user, {
        foreignKey: 'user_id'
    });

}