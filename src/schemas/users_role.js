`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`users_role`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true,
            autoIncrement: true,
        },
        user_id         : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        role_id      : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        created_at : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        status_id         : {
            type      : Sequelize.STRING,
            allowNull : false
        },
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'user'
    });
};