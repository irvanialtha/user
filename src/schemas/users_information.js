`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`users_information`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true,
            autoIncrement: true,
        },
        user_id         : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        name         : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        phone         : {
            type      : Sequelize.STRING,
            allowNull : true
        },
        address         : {
            type      : Sequelize.STRING,
            allowNull : true
        },
        city         : {
            type      : Sequelize.STRING,
            allowNull : true
        },
        zip         : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        email         : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        created_at : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        created_by         : {
            type      : Sequelize.STRING,
            allowNull : true
        },
        status_id         : {
            type      : Sequelize.STRING,
            allowNull : false
        },
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'user'
    });
};