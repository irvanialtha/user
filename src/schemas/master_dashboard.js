`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`master_dashboard`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true
        },
        dashboard : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        description         : {
            type      : Sequelize.STRING,
            allowNull : false
        }
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'user'
    });

};