`use strict`;

module.exports = {
    create: async(data) => {
        
        //** Check Email Are Exist  */
        let datauser  = {
            email : data.body.email
        }
        let checkUser = await model.user.getuser(datauser);
        if (checkUser.data.length > 0) {
            data.body = {
                            error : true,
                            message : 'Email Already Registered'
                        };
            amqp.publish(data.source,'user.output',data);
            return;
        }

        //** Create Id  */
        let id = await model.user.createid();
        if (id.error) {
            data.body = id;
            amqp.publish(data.source,'user.output',data);
            return;
        }

        //** Create User Information  */
        data.body.userid = id.data.id;
        let userinformation = await model.user.createuserinformation(data);
        if (userinformation.error) {
            data.body = userinformation;
            amqp.publish(data.source,'user.output',data);
            return;
        }

        //** Create Role  */
        let datarole = {
            userid      : data.body.userid,
            role        : data.body.role
        }
        let userrole = await model.user.createrole(datarole);
        if (userrole.error) {
            data.body = userrole;
            amqp.publish(data.source,'user.output',data);
            return;
        }

        //** Create User Password  */
        let datapassword = {
            userid      : data.body.userid,
            password    : data.body.password
        }
        let userpassword = await model.user.generatepassword(datapassword);
        if (userpassword.error) {
            data.body = userpassword;
            amqp.publish(data.source,'user.output',data);
            return;
        } else {
            data.body = { 
                success : true,
                message : 'User Created'
            }  
            amqp.publish(data.source,'user.output',data);
        }
    },
    
    getuserinfo: async(data) => {
        let datauser  = {
            userid : data.headers.env.userinfo.user_id
        }
        let userinfo = await model.user.getuser(datauser);

        data.body = {
            success : true,
            data :  {
                roles       : userinfo.data.role,
                introduction: `Welcome Back! ${userinfo.data.userinfo.name}`,
                avatar      : 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
                name        : userinfo.data.userinfo.name,
                userinfo    : userinfo.data.userinfo,
                dashboards  : userinfo.data.dashboard,
                clients     : userinfo.data.client
            }
        }
        amqp.publish(data.source,'user.output',data);
    },

    getmasterrole : async(data) => {
        data.body = await model.user.getmasterrole();
        amqp.publish(data.source,'user.output',data);

    },

    list : async(data) => {
        data.body = await model.user.getuserlist();
        amqp.publish(data.source,'user.output',data);

    }
};