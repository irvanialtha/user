`use strict`;

module.exports = {
    info: (data) =>{
        control.user.getuserinfo(data)
    },

    role: async (data) =>{
        control.user.getmasterrole(data)
    },

    create: async (data) =>{
        control.user.create(data)
    },

    list: async (data) =>{
        control.user.list(data)
    }
}