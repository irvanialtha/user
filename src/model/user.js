`use strict`;
const 
    crypto = require(`crypto-js`),
    bcrypt = require('bcrypt');
const { role } = require('../subscriber/user');

module.exports = {
    createid:()=>{
        return new Promise((resolve,reject) =>{
            db.users.create({
                status_id: 1
            })
            .then((row) => {
                resolve({
                    success : true,
                    data : row.get({ plain: true })
                })
            })
            .catch((err) => {
                console.log(err)
                resolve({
                    error : true,
                    message : 'Failed Insert user ID'
                })
            });
        })
    },
    createuserinformation:(data)=>{
        return new Promise((resolve,reject) =>{
            let params = data.body;
            db.users_information.create({
                user_id     : params.userid     || '',
                name        : params.name       || '',
                phone       : params.phone      || '',
                address     : params.address    || '',
                city        : params.city       || '',
                zip         : params.zip        || '',
                email       : params.email      || '',
                status_id   : 1
            })
            .then((row) => {
                resolve({
                    success : true,
                    data : row.get({ plain: true })
                })
            })
            .catch((err) => {
                console.log(err)
                resolve({
                    error : true,
                    message : 'Failed Insert user information'
                })
            });
        })
    },
    createrole:(data)=>{
        return new Promise((resolve,reject) =>{
            db.users_role.create({
                user_id     : data.userid     || '',
                role_id     : data.role       || '',
                status_id   : 1
            })
            .then((row) => {
                resolve({
                    success : true,
                    data    : row.get({ plain: true })
                })
            })
            .catch((err) => {
                console.log(err)
                resolve({
                    error : true,
                    message : 'Failed generate password'
                })
            });
        })
    },
    generatepassword:(data)=>{
        return new Promise((resolve,reject) =>{
            const   
                saltRounds  = 10;

            bcrypt.hash(data.password, saltRounds, function(err, hash) {
                db.users_password.create({
                    user_id     : data.userid     || '',
                    password    : hash            || '',
                    status_id   : 1
                })
                .then((row) => {
                    resolve({
                        success : true,
                        data    : row.get({ plain: true })
                    })
                })
                .catch((err) => {
                    console.log(err)
                    resolve({
                        error : true,
                        message : 'Failed generate password'
                    })
                });
            })
        })
    },
    getuser: (data)=>{

        // db.client.findAll({})
        // .then((result) =>  {
        //     console.log(result);
        // });

        let _where = {},
            output = {
                success : true,
                data : []
            }
        return new Promise((resolve,reject) =>{
            if(data.email){
                _where = {
                            status_id : {
                                [Op.eq]: 1
                            },
                            email : {
                                [Op.substring]: data.email
                            }
                        }
            }
            if(data.userid){
                _where = {
                            status_id : {
                                [Op.eq]: 1
                            },
                            user_id : {
                                [Op.eq]: data.userid
                            }
                        }
            }
            db.users.findOne({
                attributes : ['id'],
                include: [
                    {
                        model: db.users_information,
                        required: true,
                        where: _where,
                        attributes : ['name','phone','address','city','zip','email'],
                    },{
                        model: db.users_role,
                        required: true,
                        where: {
                            status_id : {
                                [Op.eq]: 1
                            }
                        }
                    },{
                        model: db.users_dashboard,
                        required: false,
                        where: {
                            status_id : {
                                [Op.eq]: 1
                            }
                        }
                    },
                    {
                        model: db.client_user,
                        required: false,
                        where: {
                            status_id : {
                                [Op.eq]: 1
                            }
                        }
                    }
                ]
            }).then(async (rows) =>{
                if (!rows) {
                    resolve(output)
                    return;
                }

                let userinfo   = {
                    userid          : rows.dataValues.id,
                    userinfo        : {},
                    role            : [],
                    roleDescription : [],
                    client          : [],
                    clientDescription: [],
                    dashboard            : [],
                    dashboardDescription : []
                }
                
                //** get userinfo */
                let userinformations = rows.dataValues.users_informations;
                for (let i in userinformations) {
                    userinfo.userinfo = userinformations[i].dataValues;
                }

                //** get role */
                let userRole = rows.dataValues.users_roles;
                for (let i in userRole) {
                    
                    let roleDesc = await model.user.getdetailmasterrole(userRole[i].role_id);
                    userinfo.roleDescription.push(roleDesc)
                    userinfo.role.push(roleDesc.role)
                }

                //** get dashboard */
                let userDashboard = rows.dataValues.users_dashboards;
                for (let i in userDashboard) {
                    
                    let dashboardDesc = await model.user.getdetailmasterdashboard(userDashboard[i].dashboard_id);
                    userinfo.dashboardDescription.push(dashboardDesc)
                    userinfo.dashboard.push(dashboardDesc.dashboard)
                }

                //** get client */
                let userClient = rows.dataValues.clients_users;
                for (let i in userClient) {
                    
                    let clientDesc = await model.user.getdetailmasterclient(userClient[i].client_id);
                    userinfo.clientDescription.push(clientDesc)
                    userinfo.client.push(clientDesc.id)
                }
                resolve({
                    success : true,
                    data    : userinfo
                })
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },
    getdetailmasterrole: (id)=>{
        let output = {};
        return new Promise((resolve,reject) =>{
            db.master_role.findOne({
                attributes : ['role', 'description'],
                where:{
                    id :{
                        [Op.eq] : id
                    }
                }
            }).then((rows) =>{
                if (!rows) {
                    resolve(output)
                    return;
                }
                resolve(rows.dataValues)
                console.log()
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },
    getmasterrole: (data)=>{
        return new Promise((resolve,reject) =>{
            db.master_role.findAll({
                order: [
                    ['id', 'ASC']
                ],
            }).then((rows) =>{
                let datalist = {
                                success : true,
                                data : []
                            }
                for (let i in rows) {
                    datalist.data.push({
                                        id: rows[i].id,
                                        role: rows[i].description,
                                        description: rows[i].url
                                    });
                }
                resolve(datalist)
            }).catch((error) =>{
                let datalist = {
                    success : true,
                    data : []
                }
                resolve(datalist)
            })
        })
    },

    getdetailmasterdashboard: (id)=>{
        let output = {};
        return new Promise((resolve,reject) =>{
            db.master_dashboard.findOne({
                attributes : ['dashboard', 'description'],
                where:{
                    id :{
                        [Op.eq] : id
                    }
                }
            }).then((rows) =>{
                if (!rows) {
                    resolve(output)
                    return;
                }
                resolve(rows.dataValues)
                console.log()
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },

    getdetailmasterclient: (id)=>{
        let output = {};
        return new Promise((resolve,reject) =>{
            db.client.findOne({
                attributes : ['id', 'client_code'],
                where:{
                    id :{
                        [Op.eq] : id
                    }
                }
            }).then((rows) =>{
                if (!rows) {
                    resolve(output)
                    return;
                }
                resolve(rows.dataValues)
                console.log()
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },

    getuserlist: (data)=>{
        let output = {
                success : true,
                data    : []
            }
        return new Promise((resolve,reject) =>{
            db.users.findAll({
                attributes : ['id'],
                include: [
                    {
                        model: db.users_information,
                        required: true,
                        attributes : ['name','phone','address','city','zip','email'],
                        where: {
                            status_id : {
                                [Op.eq]: 1
                            }
                        }
                    },{
                        model: db.users_role,
                        required: true,
                        where: {
                            status_id : {
                                [Op.eq]: 1
                            }
                        },
                        include: [
                            {
                                model: db.master_role,
                                required: true,
                                attributes : ['role','description'],
                            }
                        ]

                    }
                ]
            }).then(async (rows) =>{
                if (!rows) {
                    resolve(output)
                    return;
                }
                for (let i in rows) {
                    let result = rows[i].dataValues;
                    output.data.push({
                        userid           : result.id,
                        userinformations : result.users_informations[0],
                        role             : result.users_roles[0].master_role
                    });
                }
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },
}