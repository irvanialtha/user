/**
 * @file        : json.js
 * @description : JSON helper
 */

`use strict`;

module.exports = {

    /**
     * Check if input is JSON parsable
     * @param  {Any} obj => Source data in any data type
     * @return {Boolean}
     */
    check : (obj) => {
        try {
            obj = JSON.parse(obj);
        }
        catch(error) {
            throw new Error;
        }

        return obj;
    },

    /**
     * Check if input is JSON parsable
     * @param  {Any} obj => Source data in any data type
     * @return {Boolean}
     */
    parsable : (obj) => {
        try {
            obj = JSON.parse(obj);
        }
        catch(error) {
            return false;
        }

        return true;
    },

    /**
     * Custom stringify input
     * @param  {Any} data => Source data in any data type
     * @return {String}
     */
    stringify(data) { 
        let strdata = '';

        if (data === undefined) {
            strdata = `undefined`;
        }
        else if (data === null) {
            strdata = `null`;
        }
        else if (data.constructor === Object) {
            if (data instanceof Date) {
                strdata = data.toString();
            }
            else if (data instanceof Array) {
                strdata += `[`;

                for (let i in data) {
                    strdata += module.exports.stringify(data[i]) + `,`;
                }

                strdata = strdata.substr(0, strdata.length - 1);
                strdata += `]`;
            }
            else {
                try {
                    strdata = JSON.stringify(data);
                }
                catch(error) {
                    try {
                        strdata += `{`;

                        for (let i in data) {
                            strdata += `"${i}" : "${module.exports.stringify(data[i])}",`
                        }

                        strdata += `}`;
                    }
                    catch(error) {
                        strdata = data.toString();
                    }
                }
            }
        }
        else {
            strdata = data.toString();
        }

        return strdata;
    }
}